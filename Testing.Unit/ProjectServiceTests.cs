using System;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Xunit;
using AutoMapper;
using FakeItEasy;
using Testing.DAL.Context;
using Testing.DAL.Entities;
using Testing.BLL.Interfaces;
using Testing.BLL.Services;
using Testing.BLL.Validators.User;
using Testing.BLL.Validators.Project;
using Testing.Common.Enums;
using Testing.Common.Exceptions;
using Testing.Common.DTO.Project;

namespace Testing.Unit
{
    public sealed class ProjectServiceTests : IDisposable
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _context;
        private readonly IProjectService _projectService;
        private const int BASIC_TASKS_COUNT = 2;

        public ProjectServiceTests()
        {
            (_mapper, _context, _, _) = TestUtils.GenerateCommonServiceArgs();

            IUserService userService = new UserService(
                _context,
                _mapper,
                new CreateUserValidator(),
                new UpdateUserValidator(),
                A.Fake<TeamService>()
            );

            _projectService = new ProjectService(
                _context,
                _mapper,
                new CreateProjectValidator(),
                new UpdateProjectValidator(),
                A.Fake<TeamService>(),
                userService
            );
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public async Task GetWithTasksCount_WhenAllIsValid_ThenReturnRightData()
        {
            var (user, _, _, _) = await SeedBasicData();

            IEnumerable<ProjectWithTasksCountDTO> results = await _projectService.GetWithTasksCount(user.Id);
            int actual = results.First().TasksCount;

            Assert.Equal(BASIC_TASKS_COUNT, actual);
        }

        [Fact]
        public async Task GetWithTasksCount_WhenUnexistingUser_ThenThrow()
        {
            await Assert.ThrowsAsync<NotFoundException>(
                async () => await _projectService.GetWithTasksCount(99));
        }

        [Theory]
        [InlineData(BASIC_TASKS_COUNT, 1)]
        [InlineData(3, 21)]
        [InlineData(BASIC_TASKS_COUNT, 21)]
        public async Task GetAdditionalInfo_WhenCanGetUsersCount_ThenGetIt(uint tasksCount, uint projectDescLength)
        {
            var (_, _, project, tasks) = await SeedBasicData(tasksCount, projectDescLength);
            ProjectAdditionalInfoDTO actual = await _projectService.GetAdditionalInfo(project.Id);

            BasicGetAdditionalInfoTest(project.Id, tasks, actual);
            Assert.Equal(1, actual.UsersCountInTeam);
        }

        [Fact]
        public async Task GetAdditionalInfo_WhenCantGetUsersCount_ThenDontGetIt()
        {
            var (_, _, project, tasks) = await SeedBasicData(4);
            ProjectAdditionalInfoDTO actual = await _projectService.GetAdditionalInfo(project.Id);

            BasicGetAdditionalInfoTest(project.Id, tasks, actual);
            Assert.Null(actual.UsersCountInTeam);
        }

        [Fact]
        public async Task GetAdditionalInfo_WhenUnexistingProject_ThenThrow()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await _projectService.GetAdditionalInfo(99));
        }

        private void BasicGetAdditionalInfoTest(int projectId, IList<TaskModel> tasks, ProjectAdditionalInfoDTO actual)
        {
            Assert.Equal(projectId, actual.Project.Id);
            Assert.Equal(tasks.OrderBy(t => t.Name.Length).First().Id, actual.ShortestNameTask.Value.Id);
            Assert.Equal(tasks.OrderByDescending(t => t.Description.Length).First().Id, actual.LongestDescriptionTask.Value.Id);
        }

        private async Task<(User, Team, Project, IList<TaskModel>)> SeedBasicData(
            uint tasksCount = BASIC_TASKS_COUNT,
            uint projectDescriptionRequiredLength = 1
        )
        {
            string projectDescriptionPart = "Word. ";
            StringBuilder projectDescriptionBuilder = new StringBuilder();

            int repeatProjectDescription = (int)Math.Ceiling(
                (double)projectDescriptionRequiredLength / (double)projectDescriptionPart.Length);

            for (int i = 0; i < repeatProjectDescription; i++)
            {
                projectDescriptionBuilder.Append(projectDescriptionPart);
            }

            Team team = new Team { Name = "Test team" };
            _context.Teams.Add(team);

            User user = new User
            {
                Email = "test@test.com",
                FirstName = "Test",
                LastName = "Test",
                TeamId = team.Id,
                BirthDay = DateTime.UtcNow.AddYears(-20),
            };

            _context.Users.Add(user);

            Project project = new Project
            {
                Name = "Super project",
                Description = projectDescriptionBuilder.ToString(),
                AuthorId = user.Id,
                TeamId = team.Id,
                Deadline = DateTime.UtcNow.AddMonths(1),
            };

            _context.Projects.Add(project);
            List<TaskModel> tasks = new List<TaskModel>();

            for (int i = 0; i < tasksCount; i++)
            {
                double numId = Math.Pow(i, 4);

                TaskModel task = new TaskModel
                {
                    ProjectId = project.Id,
                    PerformerId = user.Id,
                    Name = $"Task #{numId}",
                    Description = $"Something... #{numId}",
                    State = TaskState.InProgress
                };

                _context.Tasks.Add(task);
                tasks.Add(task);
            }

            await _context.SaveChangesAsync();
            return (user, team, project, tasks);
        }
    }
}

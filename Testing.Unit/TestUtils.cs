using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.Configuration;
using FakeItEasy;
using Testing.BLL.MappingProfiles;
using Testing.DAL.Context;
using Testing.DAL.Entities;

namespace Testing.Unit
{
    public static class TestUtils
    {
        public static (IMapper, DatabaseContext, MapperConfigurationExpression, DbContextOptionsBuilder<DatabaseContext>) GenerateCommonServiceArgs()
        {
            MapperConfigurationExpression mapperConfig = new MapperConfigurationExpression();
            mapperConfig.AddProfile<UserProfile>();
            mapperConfig.AddProfile<TaskProfile>();
            mapperConfig.AddProfile<TeamProfile>();
            mapperConfig.AddProfile<ProjectProfile>();

            DbContextOptionsBuilder<DatabaseContext> contextConfig = new DbContextOptionsBuilder<DatabaseContext>();
            contextConfig.UseInMemoryDatabase("TestDB");

            IMapper mapper = new Mapper(new MapperConfiguration(mapperConfig));
            DatabaseContext context = new DatabaseContext(contextConfig.Options, false);

            context.Database.EnsureCreated();
            return (mapper, context, mapperConfig, contextConfig);
        }

        public static DbSet<T> FakeDbSet<T>(DbSet<T> existingDbSet) where T : class, IEntity, new()
        {
            return A.Fake<DbSet<T>>(opt => opt
                .Implements<IAsyncEnumerable<T>>()
                .Implements<IEnumerable<T>>()
                .Implements<IQueryable<T>>()
                .ConfigureFake(fake =>
                {
                    A.CallTo(() => ((IAsyncEnumerable<T>)fake).GetAsyncEnumerator(default))
                        .Returns(((IAsyncEnumerable<T>)existingDbSet).GetAsyncEnumerator(default));

                    A.CallTo(() => ((IEnumerable<T>)fake).GetEnumerator())
                        .Returns(((IEnumerable<T>)existingDbSet).GetEnumerator());

                    A.CallTo(() => ((IQueryable<T>)fake).Provider)
                        .Returns(((IQueryable<T>)existingDbSet).Provider);

                    A.CallTo(() => ((IQueryable<T>)fake).Expression)
                        .Returns(((IQueryable<T>)existingDbSet).Expression);

                    A.CallTo(() => ((IQueryable<T>)fake).ElementType)
                        .Returns(((IQueryable<T>)existingDbSet).ElementType);

                    A.CallTo(() => fake.EntityType)
                        .Returns(existingDbSet.EntityType);
                }));
        }
    }
}

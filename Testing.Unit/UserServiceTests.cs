using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Xunit;
using AutoMapper;
using FluentValidation;
using FakeItEasy;
using Testing.BLL.Interfaces;
using Testing.BLL.Services;
using Testing.BLL.Validators.User;
using Testing.BLL.Validators.Team;
using Testing.DAL.Entities;
using Testing.DAL.Context;
using Testing.Common.DTO.User;
using Testing.Common.Enums;
using Testing.Common.Exceptions;

namespace Testing.Unit
{
    public sealed class UserServiceTests : IDisposable
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _context;
        private readonly IUserService _userService;
        private readonly ITeamService _teamService;
        private readonly DbContextOptionsBuilder<DatabaseContext> _contextConfig;
        private const int BASIC_USERS_COUNT = 3;
        private const int BASIC_TASKS_COUNT = 2;

        public UserServiceTests()
        {
            (_mapper, _context, _, _contextConfig) = TestUtils.GenerateCommonServiceArgs();

            _teamService = new TeamService(
                _context,
                _mapper,
                new CreateTeamValidator(),
                new UpdateTeamValidator()
            );

            _userService = new UserService(
                _context,
                _mapper,
                new CreateUserValidator(),
                new UpdateUserValidator(),
                _teamService
            );
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public async Task GetWithTasksOrderedByFirstName_WhenAllIsValid_ThenReturnRightData()
        {
            IList<User> users = (await SeedBasicData()).Item2
                .OrderBy(u => u.FirstName)
                .ToList();

            List<UserWithTasksDTO> actualArray = (await _userService.GetWithTasksOrderedByFirstName()).ToList();

            for (int i = 0; i < BASIC_USERS_COUNT; i++)
            {
                User user = users[i];
                Assert.Equal(user.Id, actualArray[i].Id);

                List<TaskModel> tasks = _context.Tasks
                    .Where(t => t.PerformerId == user.Id)
                    .OrderByDescending(t => t.Name.Length)
                    .ToList();

                for (int j = 0; j < BASIC_TASKS_COUNT; j++)
                {
                    TaskModel task = tasks[j];
                    Assert.Equal(task.Id, actualArray[i].Tasks.ToList()[j].Id);
                }
            }
        }

        [Fact]
        public async Task GetAdditionalInfo_WhenAllIsValid_ThenReturnRightData()
        {
            await SeedBasicData();

            TaskModel lastTask = await _context.Tasks
                .Where(t => t.PerformerId == 1 && t.FinishedAt.HasValue)
                .OrderByDescending(t => t.FinishedAt - t.CreatedAt)
                .FirstAsync();

            UserAdditionalInfoDTO actual = await _userService.GetAdditionalInfo(1);

            Assert.Equal(1, actual.User.Id);
            Assert.Equal(1, actual.LastProject.Value.Id);
            Assert.Equal(BASIC_USERS_COUNT * BASIC_TASKS_COUNT, actual.LastProjectTasksCount);
            Assert.Equal(BASIC_TASKS_COUNT / 2, actual.UnfinishedTasksCount);
            Assert.Equal(lastTask.Id, actual.LongestTask.Value.Id);
        }

        [Fact]
        public async Task GetAdditionalInfo_WhenNoProjectAndTask_ThenReturnRightData()
        {
            User user = new User
            {
                Email = "test@test.com",
                FirstName = "User",
                LastName = "Last",
                BirthDay = DateTime.UtcNow,
            };

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            UserAdditionalInfoDTO actual = await _userService.GetAdditionalInfo(1);

            Assert.Equal(1, actual.User.Id);
            Assert.False(actual.LastProject.HasValue);
            Assert.Equal(0, actual.LastProjectTasksCount);
            Assert.Equal(0, actual.UnfinishedTasksCount);
            Assert.False(actual.LongestTask.HasValue);
        }

        [Fact]
        public async Task GetAdditionalInfo_WhenUnexistingUser_ThenThrow()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await _userService.GetAdditionalInfo(99));
        }

        [Fact]
        public async Task Create_WhenAllIsValid_ThenCreate()
        {
            DatabaseContext fakeContext = A.Fake<DatabaseContext>(opt => opt
                .WithArgumentsForConstructor(new object[] { _contextConfig.Options, false }));

            DbSet<User> fakeUsers = TestUtils.FakeDbSet<User>(_context.Users);

            A.CallTo(() => fakeContext.Users)
                .Returns(fakeUsers);

            CreateUserDTO data = new CreateUserDTO
            {
                Email = "test@test.com",
                FirstName = "User",
                LastName = "Last",
                BirthDay = DateTime.UtcNow.AddYears(-30),
            };

            IUserService newUserService = new UserService(
                fakeContext,
                _mapper,
                new CreateUserValidator(),
                new UpdateUserValidator(),
                _teamService
            );

            await newUserService.Create(data);
            A.CallTo(() => fakeContext.Users.Add(A<User>._)).MustHaveHappenedOnceExactly();
        }

        [Theory]
        [InlineData("invalidemail", "User", "Last", -30)]
        [InlineData("test@test.com", "", "Last", -30)]
        [InlineData("test@test.com", "User", "", -30)]
        [InlineData("test@test.com", "User", "Last", 30)]
        public async Task Create_WhenDataIsInvalid_ThenThrow(string email, string firstName, string lastName, int birthYears)
        {
            CreateUserDTO data = new CreateUserDTO
            {
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                BirthDay = DateTime.UtcNow.AddYears(birthYears),
            };

            await Assert.ThrowsAsync<ValidationException>(async () =>
                await _userService.Create(data));
        }

        [Fact]
        public async Task AddUserToTeam_Create_WhenExistingTeam_ThenCreate()
        {
            _context.Teams.Add(new Team { Name = "Test team" });
            await _context.SaveChangesAsync();

            DatabaseContext fakeContext = A.Fake<DatabaseContext>(opt => opt
                .WithArgumentsForConstructor(new object[] { _contextConfig.Options, false }));

            DbSet<User> fakeUsers = TestUtils.FakeDbSet<User>(_context.Users);
            DbSet<Team> fakeTeams = TestUtils.FakeDbSet<Team>(_context.Teams);

            A.CallTo(() => fakeContext.Users)
                .Returns(fakeUsers);

            A.CallTo(() => fakeContext.Teams)
                .Returns(fakeTeams);

            CreateUserDTO data = new CreateUserDTO
            {
                Email = "test@test.com",
                FirstName = "User",
                LastName = "Last",
                BirthDay = DateTime.UtcNow.AddYears(-30),
                TeamId = 1,
            };

            IUserService newUserService = new UserService(
                fakeContext,
                _mapper,
                new CreateUserValidator(),
                new UpdateUserValidator(),
                _teamService
            );

            await newUserService.Create(data);
            A.CallTo(() => fakeContext.Users.Add(A<User>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task AddUserToTeam_Create_WhenUnexistingTeam_ThenThrow()
        {
            CreateUserDTO data = new CreateUserDTO
            {
                Email = "test@test.com",
                FirstName = "User",
                LastName = "Last",
                BirthDay = DateTime.UtcNow.AddYears(-30),
                TeamId = 99,
            };

            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await _userService.Create(data));
        }

        [Fact]
        public async Task AddUserToTeam_Update_WhenExistingTeam_ThenUpdate()
        {
            _context.Teams.Add(new Team { Name = "Test team" });

            User user = new User
            {
                Email = "test@test.com",
                FirstName = "User",
                LastName = "Last",
                BirthDay = DateTime.UtcNow.AddYears(-30),
            };

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            DatabaseContext fakeContext = A.Fake<DatabaseContext>(opt => opt
                .WithArgumentsForConstructor(new object[] { _contextConfig.Options, false }));

            DbSet<User> fakeUsers = TestUtils.FakeDbSet<User>(_context.Users);
            DbSet<Team> fakeTeams = TestUtils.FakeDbSet<Team>(_context.Teams);

            A.CallTo(() => fakeContext.Users)
                .Returns(fakeUsers);

            A.CallTo(() => fakeContext.Teams)
                .Returns(fakeTeams);

            IUserService newUserService = new UserService(
                fakeContext,
                _mapper,
                new CreateUserValidator(),
                new UpdateUserValidator(),
                _teamService
            );

            user.TeamId = 1;

            await newUserService.Update(user.Id, user);
            A.CallTo(() => fakeContext.Users.Update(A<User>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task AddUserToTeam_Update_WhenUnexistingTeam_ThenThrow()
        {
            User user = new User
            {
                Email = "test@test.com",
                FirstName = "User",
                LastName = "Last",
                BirthDay = DateTime.UtcNow.AddYears(-30),
            };

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            user.TeamId = 99;

            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await _userService.Update(user.Id, user));
        }

        private async Task<(Project, IList<User>)> SeedBasicData()
        {
            List<User> users = new List<User>();
            Project project = new Project();

            for (int i = 0; i < BASIC_USERS_COUNT; i++)
            {
                User user = new User
                {
                    Email = "test@test.com",
                    FirstName = $"User {(char)(i + 97)}",
                    LastName = "Last",
                    BirthDay = DateTime.UtcNow,
                };

                _context.Users.Add(user);

                if (i == 0)
                {
                    project = new Project
                    {
                        Name = "Super project",
                        Description = "Something...",
                        Deadline = DateTime.UtcNow.AddMonths(1),
                        AuthorId = 1,
                    };

                    _context.Projects.Add(project);
                }

                for (int j = 0; j < BASIC_TASKS_COUNT; j++)
                {
                    TaskModel task = new TaskModel
                    {
                        Name = $"Task {new String(j.ToString()[0], j)}",
                        Description = $"Something...",
                        ProjectId = project.Id,
                        PerformerId = user.Id,
                        State = j % 2 == 0 ? TaskState.Done : TaskState.InProgress,
                        FinishedAt = j % 2 == 0 ? DateTime.UtcNow.AddDays((i + 1) * (j + 1)) : null,
                    };

                    _context.Tasks.Add(task);
                }

                users.Add(user);
            }

            await _context.SaveChangesAsync();
            return (project, users);
        }
    }
}

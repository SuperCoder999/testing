using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Xunit;
using AutoMapper;
using FakeItEasy;
using Testing.BLL.Interfaces;
using Testing.BLL.Services;
using Testing.BLL.Validators.Task;
using Testing.BLL.Validators.User;
using Testing.BLL.Validators.Project;
using Testing.DAL.Entities;
using Testing.DAL.Context;
using Testing.Common.DTO.Task;
using Testing.Common.Enums;
using Testing.Common.Exceptions;

namespace Testing.Unit
{
    public sealed class TaskServiceTests : IDisposable
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _context;
        private readonly ITaskService _taskService;
        private readonly IUserService _userService;
        private readonly IProjectService _projectService;
        private readonly DbContextOptionsBuilder<DatabaseContext> _contextConfig;
        private const int BASIC_TASKS_COUNT = 8;

        public TaskServiceTests()
        {
            (_mapper, _context, _, _contextConfig) = TestUtils.GenerateCommonServiceArgs();

            _userService = new UserService(
                _context,
                _mapper,
                new CreateUserValidator(),
                new UpdateUserValidator(),
                A.Fake<TeamService>()
            );

            _projectService = new ProjectService(
                _context,
                _mapper,
                new CreateProjectValidator(),
                new UpdateProjectValidator(),
                A.Fake<TeamService>(),
                _userService
            );

            _taskService = new TaskService(
                _context,
                _mapper,
                new CreateTaskValidator(),
                new UpdateTaskValidator(),
                _projectService,
                _userService
            );
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public async Task GetAssignedToWithShortName_WhenAllIsValid_ThenReturnRightData()
        {
            await SeedBasicData();
            IEnumerable<TaskDTO> actualArray = await _taskService.GetAssignedToWithShortName(1);

            Assert.Equal(BASIC_TASKS_COUNT / 2, actualArray.Count());

            foreach (TaskDTO task in actualArray)
            {
                Assert.True(task.Name.Length < 45);
            }
        }

        [Fact]
        public async Task GetAssignedToWithShortName_WhenUnexistingUser_ThenThrow()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await _taskService.GetAssignedToWithShortName(99));
        }

        [Fact]
        public async Task GetShortFinishedInCurrentYearAssignedTo_WhenAllIsValid_ThenReturnRightData()
        {
            await SeedBasicData();
            IEnumerable<TaskShortDTO> actualArray = await _taskService.GetShortFinishedInCurrentYearAssignedTo(1);

            Assert.Equal(BASIC_TASKS_COUNT / 4, actualArray.Count());
        }

        [Fact]
        public async Task GetShortFinishedInCurrentYearAssignedTo_WhenUnexistingUser_ThenThrow()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await _taskService.GetShortFinishedInCurrentYearAssignedTo(99));
        }

        [Fact]
        public async Task UpdateTaskAsDone_WhenAllIsRight_ThenUpdate()
        {
            User user = new User
            {
                Email = "test@test.com",
                FirstName = "Test",
                LastName = "Test",
                BirthDay = DateTime.UtcNow.AddYears(-20),
            };

            _context.Users.Add(user);

            Project project = new Project
            {
                Name = "Super project",
                Description = "Something...",
                AuthorId = user.Id,
                Deadline = DateTime.UtcNow.AddMonths(1),
            };

            _context.Projects.Add(project);

            TaskModel task = new TaskModel
            {
                Name = $"Task",
                Description = $"Something...",
                ProjectId = project.Id,
                PerformerId = user.Id,
                State = TaskState.InProgress,
            };

            _context.Tasks.Add(task);
            await _context.SaveChangesAsync();

            DatabaseContext fakeContext = A.Fake<DatabaseContext>(opt => opt
                .WithArgumentsForConstructor(new object[] { _contextConfig.Options, false }));

            DbSet<TaskModel> fakeTasks = TestUtils.FakeDbSet<TaskModel>(_context.Tasks);

            A.CallTo(() => fakeContext.Tasks)
                .Returns(fakeTasks);

            ITaskService newTaskService = new TaskService(
                fakeContext,
                _mapper,
                new CreateTaskValidator(),
                new UpdateTaskValidator(),
                A.Fake<ProjectService>(opt => opt
                    .ConfigureFake(fake => A.CallTo(() => fake.Exists(A<int>._)).Returns(true))),
                A.Fake<UserService>(opt => opt
                    .ConfigureFake(fake => A.CallTo(() => fake.Exists(A<int>._)).Returns(true)))
            );

            task.State = TaskState.Done;
            task.FinishedAt = DateTime.UtcNow;

            await newTaskService.Update(task.Id, task);

            A.CallTo(() => fakeContext.Tasks.Update(A<TaskModel>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeContext.SaveChangesAsync(default)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task UpdateTaskAsDone_WhenUnexistingTask_ThenThrow()
        {
            TaskModel dummyTask = new TaskModel
            {
                Name = $"Task",
                Description = $"Something...",
                ProjectId = 1,
                PerformerId = 1,
                State = TaskState.InProgress,
            };

            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await _taskService.Update(99, dummyTask));
        }

        [Fact]
        public async Task GetUnfinishedFor_WhenDifferentTasks_ThenReturnRightTasks()
        {
            await SeedBasicData();

            IEnumerable<TaskDTO> actual = await _taskService.GetUnfinishedFor(1);
            Assert.Equal(BASIC_TASKS_COUNT / 2, actual.Count());

            foreach (TaskDTO task in actual)
            {
                Assert.Null(task.FinishedAt);
            }
        }

        [Fact]
        public async Task GetUnfinishedFor_WhenUnexistingTask_ThenThrow()
        {
            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await _taskService.GetUnfinishedFor(99));
        }

        private async Task<(User, IList<Project>, IList<TaskModel>)> SeedBasicData()
        {
            User user = new User
            {
                Email = "test@test.com",
                FirstName = "Test",
                LastName = "Test",
                BirthDay = DateTime.UtcNow.AddYears(-20),
            };

            _context.Users.Add(user);

            Project project1 = new Project
            {
                Name = "Super project",
                Description = "Something...",
                AuthorId = user.Id,
                Deadline = DateTime.UtcNow.AddMonths(1),
            };

            _context.Projects.Add(project1);

            Project project2 = new Project
            {
                Name = "Super project 2",
                Description = "Something... 2",
                AuthorId = user.Id,
                Deadline = DateTime.UtcNow.AddMonths(2),
            };

            _context.Projects.Add(project2);

            List<TaskModel> tasks = new List<TaskModel>();

            for (int i = 0; i < BASIC_TASKS_COUNT; i++)
            {
                TaskModel task = new TaskModel
                {
                    Name = $"Task task task task task task task task #{i}" + (i % 2 == 0 ? " more text..." : ""),
                    Description = $"Something... #{i}",
                    ProjectId = i % 2 == 0 ? project1.Id : project2.Id,
                    PerformerId = user.Id,
                    State = TaskState.Cancelled,
                    FinishedAt = i % 2 == 0 ? DateTime.UtcNow.AddYears(i % 4 == 0 ? -1 : 0) : null,
                };

                _context.Tasks.Add(task);
                tasks.Add(task);
            }

            await _context.SaveChangesAsync();
            return (user, new List<Project> { project1, project2 }, tasks);
        }
    }
}

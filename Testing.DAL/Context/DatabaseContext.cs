using Microsoft.EntityFrameworkCore;
using Testing.DAL.Entities;

namespace Testing.DAL.Context
{
    public class DatabaseContext : DbContext
    {
        private readonly bool _seed = true;

        public DatabaseContext(bool seed = true) : base()
        {
            _seed = seed;
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> opt, bool seed = true) : base(opt)
        {
            _seed = seed;
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<TaskModel> Tasks { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<Project> Projects { get; set; }

        protected override void OnModelCreating(ModelBuilder opt)
        {
            opt.ConfigureModels();

            if (_seed)
            {
                opt.Seed();
            }

            base.OnModelCreating(opt);
        }

        /* Temporary uncomment if `dotnet ef <something>` fails */
        /*
        protected override void OnConfiguring(DbContextOptionsBuilder opt)
        {
            if (!opt.IsConfigured)
            {
                opt.UseNpgsql(Environment.GetEnvironmentVariable("DB_CONNECTION_STRING"));
            }
        }
        */
    }
}

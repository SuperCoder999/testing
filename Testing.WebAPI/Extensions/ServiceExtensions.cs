using System;
using dotenv.net;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using FluentValidation;
using FluentValidation.AspNetCore;
using Testing.DAL.Context;
using Testing.BLL.Interfaces;
using Testing.BLL.Services;
using Testing.BLL.MappingProfiles;
using Testing.BLL.Validators.User;
using Testing.BLL.Validators.Task;
using Testing.BLL.Validators.Team;
using Testing.BLL.Validators.Project;
using Testing.Common.DTO.User;
using Testing.Common.DTO.Task;
using Testing.Common.DTO.Team;
using Testing.Common.DTO.Project;
using Testing.WebAPI.Filters;

namespace Testing.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void LoadEnvironment(this IServiceCollection services)
        {
            DotEnv.Load(new DotEnvOptions(false, new string[] { "../.env" }));
        }

        public static void AddDAL(this IServiceCollection services)
        {
            string migrationsAssembly = typeof(DatabaseContext).Assembly.GetName().FullName;

            services.AddDbContext<DatabaseContext>(opt =>
                opt.UseNpgsql(
                    Environment.GetEnvironmentVariable("DB_CONNECTION_STRING"),
                    opt => opt
                        .MigrationsAssembly(migrationsAssembly)
                        .UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery)
                ));
        }

        public static void AddTestingDAL(this IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>(opt => opt
                .UseInMemoryDatabase("TestDB"));
        }

        public static void AddMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(opt =>
            {
                opt.AddProfile<UserProfile>();
                opt.AddProfile<TaskProfile>();
                opt.AddProfile<TeamProfile>();
                opt.AddProfile<ProjectProfile>();
            });
        }

        public static void AddValidation(this IServiceCollection services)
        {
            services.AddSingleton<IValidator<CreateUserDTO>, CreateUserValidator>();
            services.AddSingleton<IValidator<UpdateUserDTO>, UpdateUserValidator>();

            services.AddSingleton<IValidator<CreateTaskDTO>, CreateTaskValidator>();
            services.AddSingleton<IValidator<UpdateTaskDTO>, UpdateTaskValidator>();

            services.AddSingleton<IValidator<CreateTeamDTO>, CreateTeamValidator>();
            services.AddSingleton<IValidator<UpdateTeamDTO>, UpdateTeamValidator>();

            services.AddSingleton<IValidator<CreateProjectDTO>, CreateProjectValidator>();
            services.AddSingleton<IValidator<UpdateProjectDTO>, UpdateProjectValidator>();
        }

        public static void AddBLL(this IServiceCollection services)
        {
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<ITaskService, TaskService>();
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(opt =>
            {
                opt.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Testing API",
                    Version = "1.0.0"
                });
            });
        }

        public static void AddAPI(this IServiceCollection services)
        {
            services
                .AddControllers(opt => opt.Filters.Add(typeof(CustomExceptionFilterAttribute)))
                .AddNewtonsoftJson()
                .AddFluentValidation();
        }

        public static void ConfigureDevelopmentServices(this IServiceCollection services)
        {
            services.LoadEnvironment();
            services.AddDAL();
            services.AddMapper();
            services.AddValidation();
            services.AddBLL();
            services.AddCors();
            services.AddAPI();
            services.AddSwagger();
        }

        public static void ConfigureTestingServices(this IServiceCollection services)
        {
            services.AddTestingDAL();
            services.AddMapper();
            services.AddValidation();
            services.AddBLL();
            services.AddCors();
            services.AddAPI();
            services.AddSwagger();
        }
    }
}

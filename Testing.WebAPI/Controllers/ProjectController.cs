using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Testing.DAL.Entities;
using Testing.BLL.Interfaces;
using Testing.Common.DTO.Project;

namespace Testing.WebAPI.Controllers
{
    [ApiController]
    [Route("api/projects")]
    public sealed class ProjectController
        : AbstractController<IProjectService, Project, ProjectDTO, CreateProjectDTO, UpdateProjectDTO>
    {
        public ProjectController(IProjectService service) : base(service) { }

        [HttpGet("with-tasks-count/{userId}")]
        public async Task<ActionResult<IEnumerable<ProjectWithTasksCountDTO>>> GetWithTasksCount([FromRoute] int userId)
        {
            return Ok(await _service.GetWithTasksCount(userId));
        }

        [HttpGet("{id}/additional-info")]
        public async Task<ActionResult<ProjectAdditionalInfoDTO>> GetAdditionalInfo([FromRoute] int id)
        {
            return Ok(await _service.GetAdditionalInfo(id));
        }
    }
}

using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Testing.DAL.Entities;
using Testing.BLL.Interfaces;
using Testing.Common.DTO.Task;

namespace Testing.WebAPI.Controllers
{
    [ApiController]
    [Route("api/tasks")]
    public sealed class TaskController : AbstractController<ITaskService, TaskModel, TaskDTO, CreateTaskDTO, UpdateTaskDTO>
    {
        public TaskController(ITaskService service) : base(service) { }

        [HttpGet("with-short-name/{userId}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetAssignedToWithShortName([FromRoute] int userId)
        {
            return Ok(await _service.GetAssignedToWithShortName(userId));
        }

        [HttpGet("finished-in-current-year/{userId}")]
        public async Task<ActionResult<IEnumerable<TaskShortDTO>>> GetShortFinishedInCurrentYearAssignedTo([FromRoute] int userId)
        {
            return Ok(await _service.GetShortFinishedInCurrentYearAssignedTo(userId));
        }

        [HttpGet("unfinished-for/{userId}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUnfinishedFor([FromRoute] int userId)
        {
            return Ok(await _service.GetUnfinishedFor(userId));
        }
    }
}

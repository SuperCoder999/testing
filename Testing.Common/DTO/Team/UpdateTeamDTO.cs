#nullable enable

namespace Testing.Common.DTO.Team
{
    public struct UpdateTeamDTO
    {
        public string? Name { get; set; }
    }
}

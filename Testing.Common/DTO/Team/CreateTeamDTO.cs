using Newtonsoft.Json;

namespace Testing.Common.DTO.Team
{
    public struct CreateTeamDTO
    {
        [JsonRequired] public string Name { get; set; }
    }
}

using System;
using System.Collections.Generic;
using Testing.Common.DTO.User;

namespace Testing.Common.DTO.Team
{
    public struct TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<UserDTO> Users;
    }
}

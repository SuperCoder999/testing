using System.Collections.Generic;

namespace Testing.Common.DTO
{
    public struct HttpErrorDTO
    {
        public string Error { get; set; }
    }
}

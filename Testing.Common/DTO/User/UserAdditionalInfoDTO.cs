using Testing.Common.DTO.Project;
using Testing.Common.DTO.Task;

namespace Testing.Common.DTO.User
{
    public struct UserAdditionalInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO? LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int UnfinishedTasksCount { get; set; }
        public TaskWithoutPerformerDTO? LongestTask { get; set; }
    }
}

using System;
using System.Collections.Generic;
using Testing.Common.DTO.Team;
using Testing.Common.DTO.Task;
using Testing.Common.DTO.User;

namespace Testing.Common.DTO.Project
{
    public struct ProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<TaskDTO> Tasks;
        public TeamDTO? Team { get; set; }
        public UserDTO? Author { get; set; }
    }
}

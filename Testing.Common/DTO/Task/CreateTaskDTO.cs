
using System;
using Testing.Common.Enums;
using Newtonsoft.Json;

namespace Testing.Common.DTO.Task
{
    public struct CreateTaskDTO
    {
        [JsonRequired] public int ProjectId { get; set; }
        public int? PerformerId { get; set; }
        [JsonRequired] public string Name { get; set; }
        public string Description { get; set; }
        [JsonRequired] public TaskState State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}

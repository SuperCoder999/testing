namespace Testing.Common.DTO.Task
{
    public struct TaskShortDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

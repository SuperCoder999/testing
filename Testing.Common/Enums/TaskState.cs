namespace Testing.Common.Enums
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Cancelled,
    }
}

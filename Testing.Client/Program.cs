﻿using System.Threading.Tasks;
using Testing.Client.ConsoleInterface;

namespace Testing.Client
{
    public sealed class Program
    {
        public static async Task Main(string[] args)
        {
            ConsoleMenu app = new ConsoleMenu();
            await app.Run();
        }
    }
}

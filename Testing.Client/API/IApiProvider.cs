using System;
using System.Threading.Tasks;

namespace Testing.Client.API
{
    public interface IApiProvider : IDisposable
    {
        Task<T> Get<T>(string endpoint);
    }
}

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Testing.Common.DTO.Project;
using Testing.Client.Services;

namespace Testing.Client.Commands
{
    [Command(1, "getProjectsTasksByUserId")]
    [UseParameter("userId", "userId", "invalidNumber")]
    internal sealed class GetProjectsTasksCountByUserId : BaseCommand<GetProjectsTasksCountByUserId>
    {
        private readonly ProjectService projectService = new ProjectService();

        public override async Task Invoke()
        {
            int userId = GetParameterValue<int>("userId");
            IEnumerable<ProjectWithTasksCountDTO> projects = await projectService.GetTasksCountByUserId(userId);

            foreach (ProjectWithTasksCountDTO dto in projects)
            {
                Console.WriteLine($"Project {dto.Name} has {dto.TasksCount} tasks");
            }
        }
    }
}

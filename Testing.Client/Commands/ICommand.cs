using System.Threading.Tasks;

namespace Testing.Client.Commands
{
    internal interface ICommand
    {
        int Index { get; }
        Task Invoke();
        void Reset();
        string ToString();
    }
}

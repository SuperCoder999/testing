using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Testing.Common.DTO.Task;
using Testing.Client.Services;

namespace Testing.Client.Commands
{
    [Command(3, "getShortTasksFinishedInCurrentYearAndAssignedTo")]
    [UseParameter("userId", "userId", "invalidNumber")]
    internal sealed class GetShortTasksFinishedInCurrentYearAssignedTo
        : BaseCommand<GetShortTasksFinishedInCurrentYearAssignedTo>
    {
        private readonly TaskService taskService = new TaskService();

        public override async Task Invoke()
        {
            int userId = GetParameterValue<int>("userId");
            IEnumerable<TaskShortDTO> data = await taskService.GetShortFinishedInCurrentYearAssignedTo(userId);

            foreach (TaskShortDTO dto in data)
            {
                Console.WriteLine($"Id: {dto.Id}, Name: {dto.Name}");
            }
        }
    }
}

using System.Threading.Tasks;
using System.Collections.Generic;
using Testing.Common.DTO.User;

namespace Testing.Client.Services
{
    internal sealed class UserService : AbstarctService
    {
        public UserService() : base("users") { }

        public async Task<IEnumerable<UserWithTasksDTO>> GetWithTasksOrderedByFirstName()
        {
            return await _api.Get<IEnumerable<UserWithTasksDTO>>("with-tasks");
        }

        public async Task<UserAdditionalInfoDTO> GetAdditionalInfo(int id)
        {
            return await _api.Get<UserAdditionalInfoDTO>($"{id}/additional-info");
        }
    }
}

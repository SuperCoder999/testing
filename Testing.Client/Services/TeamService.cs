using System.Threading.Tasks;
using System.Collections.Generic;
using Testing.Common.DTO.Team;

namespace Testing.Client.Services
{
    internal sealed class TeamService : AbstarctService
    {
        public TeamService() : base("teams") { }

        public async Task<IEnumerable<TeamShortDTO>> GetIdsNamesUsers()
        {
            return await _api.Get<IEnumerable<TeamShortDTO>>("ids-names-users");
        }
    }
}

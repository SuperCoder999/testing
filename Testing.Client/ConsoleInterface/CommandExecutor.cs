using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Testing.Client.Commands;

namespace Testing.Client.ConsoleInterface
{
    internal sealed class CommandExecutor
    {
        private readonly List<ICommand> commands;

        public CommandExecutor(List<ICommand> commands)
        {
            this.commands = commands;
        }

        public async Task ExecuteCommandByIndex(int index)
        {
            ICommand command = commands.Find(c => c.Index == index);

            if (command == null)
            {
                throw new InvalidOperationException("Trying to execute unexisting command");
            }

            await command.Invoke();
        }
    }
}

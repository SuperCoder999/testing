using System.Threading.Tasks;
using System.Collections.Generic;
using Testing.Common.DTO.Task;
using Testing.DAL.Entities;

namespace Testing.BLL.Interfaces
{
    public interface ITaskService : IService<TaskModel, TaskDTO, CreateTaskDTO>
    {
        Task<IEnumerable<TaskDTO>> GetAssignedToWithShortName(int userId);
        Task<IEnumerable<TaskShortDTO>> GetShortFinishedInCurrentYearAssignedTo(int userId);
        Task<IEnumerable<TaskDTO>> GetUnfinishedFor(int userId);
    }
}

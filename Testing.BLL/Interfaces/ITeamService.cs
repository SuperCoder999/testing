using System.Threading.Tasks;
using System.Collections.Generic;
using Testing.Common.DTO.Team;
using Testing.DAL.Entities;

namespace Testing.BLL.Interfaces
{
    public interface ITeamService : IService<Team, TeamDTO, CreateTeamDTO>
    {
        Task<IEnumerable<TeamShortDTO>> GetShortWithUsersOlder10();
    }
}

using System.Threading.Tasks;
using System.Collections.Generic;
using Testing.Common.DTO.User;
using Testing.DAL.Entities;

namespace Testing.BLL.Interfaces
{
    public interface IUserService : IService<User, UserDTO, CreateUserDTO>
    {
        Task<IEnumerable<UserWithTasksDTO>> GetWithTasksOrderedByFirstName();
        Task<UserAdditionalInfoDTO> GetAdditionalInfo(int id);
    }
}

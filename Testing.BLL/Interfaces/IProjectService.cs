using System.Threading.Tasks;
using System.Collections.Generic;
using Testing.Common.DTO.Project;
using Testing.DAL.Entities;

namespace Testing.BLL.Interfaces
{
    public interface IProjectService : IService<Project, ProjectDTO, CreateProjectDTO>
    {
        Task<IEnumerable<ProjectWithTasksCountDTO>> GetWithTasksCount(int userId);
        Task<ProjectAdditionalInfoDTO> GetAdditionalInfo(int id);
    }
}

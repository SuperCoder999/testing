using AutoMapper;
using Testing.DAL.Entities;
using Testing.Common.DTO.Task;

namespace Testing.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<CreateTaskDTO, TaskModel>();
            CreateMap<TaskModel, UpdateTaskDTO>();
            CreateMap<TaskModel, TaskDTO>();
            CreateMap<TaskModel, TaskWithoutPerformerDTO>();
            CreateMap<TaskModel, TaskShortDTO>();
        }
    }
}

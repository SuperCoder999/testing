using System.Linq;
using AutoMapper;
using Testing.DAL.Entities;
using Testing.Common.DTO.Project;

namespace Testing.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<CreateProjectDTO, Project>();
            CreateMap<Project, UpdateProjectDTO>();
            CreateMap<Project, ProjectDTO>();

            CreateMap<Project, ProjectWithTasksCountDTO>()
                .ForMember<int>(dto => dto.TasksCount, opt => opt.MapFrom(p => p.Tasks.Count()));
        }
    }
}

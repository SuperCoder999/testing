using AutoMapper;
using Testing.DAL.Entities;
using Testing.Common.DTO.Team;

namespace Testing.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<CreateTeamDTO, Team>();
            CreateMap<Team, UpdateTeamDTO>();
            CreateMap<Team, TeamDTO>();
            CreateMap<Team, TeamShortDTO>();
        }
    }
}

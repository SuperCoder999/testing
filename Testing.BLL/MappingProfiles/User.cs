using AutoMapper;
using Testing.DAL.Entities;
using Testing.Common.DTO.User;

namespace Testing.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<CreateUserDTO, User>();
            CreateMap<User, UpdateUserDTO>();
            CreateMap<User, UserDTO>();
            CreateMap<User, UserWithTasksDTO>();
        }
    }
}

using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Hosting;
using Testing.WebAPI;

namespace Testing.Integration
{
    public sealed class WebFactory : WebApplicationFactory<TestStartup>
    {
        protected override IHostBuilder CreateHostBuilder()
        {
            return Host
                .CreateDefaultBuilder()
                .ConfigureWebHostDefaults(opt => opt.UseStartup<TestStartup>());
        }
    }
}

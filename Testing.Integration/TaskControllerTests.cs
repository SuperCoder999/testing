using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Xunit;
using Testing.Common;
using Testing.Common.Enums;
using Testing.Common.DTO.Task;
using Testing.Common.DTO.User;
using Testing.Common.DTO.Project;

namespace Testing.Integration
{
    public sealed class TaskControllerTests : IClassFixture<WebFactory>, IDisposable
    {
        private readonly HttpClient _client;

        public TaskControllerTests(WebFactory factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task Delete_WhenAllIsRight_ThenDelete()
        {
            CreateProjectDTO data = new CreateProjectDTO
            {
                Name = "New project",
                Description = "Loooong descripton",
                Deadline = DateTime.UtcNow.AddDays(100),
            };

            HttpResponseMessage projectMessage = await _client.PostAsync(
                "api/projects",
                Utils.ConstructJsonRequestContent(data)
            );

            ProjectDTO project = await Utils.DeserializeResponse<ProjectDTO>(projectMessage);

            CreateTaskDTO taskData = new CreateTaskDTO
            {
                ProjectId = project.Id,
                Name = "A task",
                Description = "Loooong desc",
                State = TaskState.InProgress
            };

            HttpResponseMessage taskMessage = await _client.PostAsync(
                "api/tasks",
                Utils.ConstructJsonRequestContent(taskData)
            );

            TaskDTO task = await Utils.DeserializeResponse<TaskDTO>(taskMessage);

            HttpResponseMessage deleteMessage = await _client.DeleteAsync($"api/tasks/{task.Id}");
            Assert.True(deleteMessage.IsSuccessStatusCode);

            HttpResponseMessage checkMessage = await _client.GetAsync($"api/tasks/{task.Id}");
            Assert.False(checkMessage.IsSuccessStatusCode);
        }

        [Fact]
        public async Task Delete_WhenUnexistingTask_ThenThrow()
        {
            HttpResponseMessage deleteMessage = await _client.DeleteAsync("api/tasks/99");
            Assert.False(deleteMessage.IsSuccessStatusCode);
        }

        [Fact]
        public async Task GetUnfinishedFor_WhenDifferentTasks_ThenReturnRightTasks()
        {
            CreateUserDTO userData = new CreateUserDTO
            {
                FirstName = "Test",
                LastName = "Test",
                Email = "test@test.com",
                BirthDay = DateTime.UtcNow.AddYears(-30),
            };

            HttpResponseMessage userMessage = await _client.PostAsync(
                "api/users",
                Utils.ConstructJsonRequestContent(userData)
            );

            UserDTO user = await Utils.DeserializeResponse<UserDTO>(userMessage);
            IList<ProjectDTO> projects = new List<ProjectDTO>();

            for (int i = 0; i < 2; i++)
            {
                CreateProjectDTO projectData = new CreateProjectDTO
                {
                    Name = "New project",
                    Description = "Loooong descripton",
                    Deadline = DateTime.UtcNow.AddDays(100),
                    AuthorId = user.Id,
                };

                HttpResponseMessage projectMessage = await _client.PostAsync(
                    "api/projects",
                    Utils.ConstructJsonRequestContent(projectData)
                );

                ProjectDTO project = await Utils.DeserializeResponse<ProjectDTO>(projectMessage);
                projects.Add(project);
            }

            for (int i = 0; i < 6; i++)
            {
                CreateTaskDTO taskData = new CreateTaskDTO
                {
                    ProjectId = projects[i % 2].Id,
                    PerformerId = user.Id,
                    Name = "A task",
                    Description = "Loooong desc",
                    State = TaskState.InProgress,
                    FinishedAt = i % 2 == 0 ? DateTime.UtcNow.AddHours(-1) : null,
                };

                await _client.PostAsync(
                    "api/tasks",
                    Utils.ConstructJsonRequestContent(taskData)
                );
            }

            HttpResponseMessage actualMessage = await _client.GetAsync($"api/tasks/unfinished-for/{user.Id}");
            IEnumerable<TaskDTO> actual = await Utils.DeserializeResponse<IEnumerable<TaskDTO>>(actualMessage);

            Assert.Equal(3, actual.Count());

            foreach (TaskDTO task in actual)
            {
                Assert.Null(task.FinishedAt);
            }
        }

        [Fact]
        public async Task GetUnfinishedFor_WhenUnexistingUser_ThenThrow()
        {
            HttpResponseMessage actualMessage = await _client.GetAsync($"api/tasks/unfinished-for/99");
            Assert.False(actualMessage.IsSuccessStatusCode);
        }
    }
}

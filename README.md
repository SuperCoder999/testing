# Testing

## Requirements:

- .NET 5.0
- Docker
- docker-compose

## Setup:

- Create `.env` and `.database.env` based on `.env.example` and `.database.env.example`
- `docker-compose up -d`

## Start:

### Backend:

- `cd Testing.WebAPI`
- `dotnet run`

### Client:

- `cd Testing.Client`
- `dotnet run`

## DB operations:

- `cd Testing.DAL`
- `dotnet ef <something> --startup-project ../Testing.WebAPI`

### If `dotnet ef <something> --startup-project ../Testing.WebAPI` fails (can't connect to DB, etc.)

Uncomment marked lines in `Testing.DAL/Context/DatabaseContext.cs`

## Notes:

- Business logic is located at `Testing.BLL`
- Unit tests are located at `Testing.Unit`
- Integration tests are located at `Testing.Integration`
- PostgreSQL is used, because it's faster
- First DB queries are automatically cached, so may last more time and load more data
